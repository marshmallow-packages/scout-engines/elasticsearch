<?php

namespace Marshmallow\ScoutEngines\ElasticSearch;

use InvalidArgumentException;
use Elasticsearch\ClientBuilder;
use Laravel\Scout\EngineManager;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Marshmallow\ScoutEngines\ElasticSearch\Console\ReindexModelCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\ElasticMigrateCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\SearchRuleMakeCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\ElasticIndexDropCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\ElasticIndexCreateCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\ElasticIndexUpdateCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\SearchableModelMakeCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\ElasticUpdateMappingCommand;
use Marshmallow\ScoutEngines\ElasticSearch\Console\IndexConfiguratorMakeCommand;

class ScoutElasticServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/scout_elastic.php' => config_path('scout_elastic.php'),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                // make commands
                IndexConfiguratorMakeCommand::class,
                SearchableModelMakeCommand::class,
                SearchRuleMakeCommand::class,

                // elastic commands
                ElasticIndexCreateCommand::class,
                ElasticIndexUpdateCommand::class,
                ElasticIndexDropCommand::class,
                ElasticUpdateMappingCommand::class,
                ElasticMigrateCommand::class,
                ReindexModelCommand::class,
            ]);
        }


        $this
            ->app
            ->make(EngineManager::class)
            ->extend('elastic', function () {
                $indexerType = config('scout_elastic.indexer', 'single');
                $updateMapping = config('scout_elastic.update_mapping', true);

                $indexerClass = '\\Marshmallow\\ScoutEngines\\ElasticSearch\\Indexers\\'.ucfirst($indexerType).'Indexer';

                if (! class_exists($indexerClass)) {
                    throw new InvalidArgumentException(sprintf(
                        'The %s indexer doesn\'t exist.',
                        $indexerType
                    ));
                }

                return new ElasticEngine(new $indexerClass, $updateMapping);
            });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/scout_elastic.php', 'scout_elastic'
        );

        $this
            ->app
            ->singleton('scout_elastic.client', function () {
                $config = Config::get('scout_elastic.client');
                return ClientBuilder::fromConfig($config);
            });
    }
}
