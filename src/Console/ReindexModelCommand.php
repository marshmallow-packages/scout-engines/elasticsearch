<?php

namespace Marshmallow\ScoutEngines\ElasticSearch\Console;

use Illuminate\Console\Command;
use LogicException;
use Marshmallow\ScoutEngines\ElasticSearch\Console\Features\RequiresModelArgument;
use Marshmallow\ScoutEngines\ElasticSearch\Facades\ElasticClient;
use Marshmallow\ScoutEngines\ElasticSearch\Migratable;
use Marshmallow\ScoutEngines\ElasticSearch\Payloads\TypePayload;

class ReindexModelCommand extends Command
{
    use RequiresModelArgument;

    /**
     * {@inheritdoc}
     */
    protected $name = 'elastic:reindex';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Reindex a model mapping';

    /**
     * Handle the command.
     *
     * @return void
     */
    public function handle()
    {
        if (! $model = $this->getModel()) {
            return;
        }

        $records = $model->get();

        dd($records->count());

        $configurator = $model->getIndexConfigurator();

        $mapping = array_merge_recursive(
            $configurator->getDefaultMapping(),
            $model->getMapping()
        );

        if (empty($mapping)) {
            throw new LogicException('Nothing to update: the mapping is not specified.');
        }

        $payload = (new TypePayload($model))
            ->set('body.'.$model->searchableAs(), $mapping)
            ->set('include_type_name', 'true');

        if (in_array(Migratable::class, class_uses_recursive($configurator))) {
            $payload->useAlias('write');
        }

        ElasticClient::indices()
            ->putMapping($payload->get());

        $this->info(sprintf(
            'The %s mapping was updated!',
            $model->searchableAs()
        ));
    }
}
