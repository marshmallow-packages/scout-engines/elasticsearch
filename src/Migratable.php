<?php

namespace Marshmallow\ScoutEngines\ElasticSearch;

trait Migratable
{
    /**
     * Get the write alias.
     *
     * @return string
     */
    public function getWriteAlias()
    {
        return $this->getName().'_write';
    }
}
